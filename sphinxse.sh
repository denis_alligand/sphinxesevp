#!/bin/bash
WHITE='\e[1;37m'
LIGHTGRAY='\e[0;37m'
GRAY='\e[1;30m'
WHITE='\e[1;37m'
BLACK='\e[0;30m'
RED='\e[0;31m'
LIGHTRED='\e[1;31m'
GREEN='\e[0;32m'
LIGHTGREEN='\e[1;32m'
BROWN='\e[0;33m'
YELLOW='\e[1;33m'
CYAN='\e[0;34m'
col_LB='\e[1;34m'
PURPLE='\e[0;35m'
PINK='\e[1;35m'
CYAN='\e[0;36m'
LIGHTCYAN='\e[1;36m'
D='\e[0m'

step=1000
while getopts "b:s:h" opt; do
  case $opt in
    b)
      base=${OPTARG};
      ;;
    s)
      step=${OPTARG};
      ;;
     h)
       echo -e "${CYAN}------  Help  ------${D}";
        echo -e "${YELLOW}Options :${D}"
        echo -e "${LIGHTGREEN}-b ${D}: base"
        echo -e "${LIGHTGREEN}-s ${D}: precise l increment (defaut 1000)"
        exit 0
        ;;



    \?)
      echo "Invalid option: -$OPTARG" >&2
        exit 1
      ;;
  esac
done

max=$(echo "select max(id_resa) from resa;"|mysql $base|grep -v max)

for i in $(seq 1 $step $max)
do
	debut=$i
	fin=$(expr $i + $step - 1)
	if [ "$fin" -gt "$max" ] 
	then
		fin=$max
	fi
	echo "CALL replaceSphinxIndex($debut, $fin);" 
	$(echo "CALL replaceSphinxIndex($debut, $fin);"|mysql $base)
	sleep 5 
done
