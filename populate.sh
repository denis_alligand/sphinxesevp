#!/bin/bash
echo "START ($date)"
for BO in bo_vpfr bo_vpit bo_vpuk bo_vpsp bo_vppl bo_vpbr
do
 echo "INDEXING $BO $(date)..."
 ./sphinxse.sh -b $BO -s 500
done
echo "END ($date)"
